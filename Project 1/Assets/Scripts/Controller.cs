﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public Pawn pawn;
    private Transform player;
    public KeyCode crouchKey;
    private Text weaponText;

    // Use this for initialization
    void Start()
    {
        pawn = GetComponent<Pawn>();
        player = gameObject.transform;
        Camera.main.GetComponent<CameraFollowPlayer>().targetTf = player;
        GameManager.instance.player = this;

        weaponText = GameObject.Find("Ammo Counter").GetComponent<Text>();
        weaponText.text = "No Weapon";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.instance.TogglePause();     // Swap paused state
        }

        if (GameManager.instance.isPaused)
        {
            return;     // DO NOTHING
        }

        Rotation();
        Movement();
        if (Input.GetMouseButton(0))
        {
            if (pawn.weaponScript)
            {
                pawn.weaponScript.OnShoot();
            }
        }
    }

    void Rotation()
    {
        //Create plane
        Plane newPlane = new Plane(Vector3.up, pawn.tf.position);

        //Raycast from camera at mouse position; find distance to plane
        float distance;
        Ray newRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        newPlane.Raycast(newRay, out distance);
        //Find point on ray at that distance
        Vector3 newPoint = newRay.GetPoint(distance);

        player.LookAt(new Vector3(newPoint.x, player.position.y, newPoint.z));
    } 

    void Movement()
    {
        //Move
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1.0f);
        moveDirection = pawn.tf.InverseTransformDirection(moveDirection);

        if (Input.GetKeyDown(crouchKey))
        {
            pawn.SwapCrouch();
        }

        pawn.Move(moveDirection);
    }
}
