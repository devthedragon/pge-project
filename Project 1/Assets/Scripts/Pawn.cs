﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pawn : MonoBehaviour {

    public Animator anim;
    public Transform tf;
    public CapsuleCollider pawnCollider;

    public GameObject equippedWeapon;
    public WeaponBase weaponScript;
    public Transform weaponPoint;

    public Transform rightPoint;
    public Transform leftPoint;

    public List<Sprite> weaponSprites;
    public Image weaponImage;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
        pawnCollider = GetComponent<CapsuleCollider>();
        if (gameObject.layer == 9)
        {
            weaponImage = GameObject.Find("Weapon Image").GetComponent<Image>();
            weaponImage.sprite = weaponSprites[0];
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (equippedWeapon && GetComponent<Health>().ReturnHealth() > 0)
        {
            equippedWeapon.transform.position = weaponPoint.position;
        }
	}

    public void SwapCrouch()
    {
        if (anim.GetBool("IsCrouching") == false)
        {
            anim.SetBool("IsCrouching", true);
            pawnCollider.height = 1;
            pawnCollider.center = new Vector3(pawnCollider.center.x, 0.5f,pawnCollider.center.z);
            weaponPoint.transform.localPosition = new Vector3(weaponPoint.transform.localPosition.x, weaponPoint.transform.localPosition.y / 2, weaponPoint.transform.localPosition.z);
            equippedWeapon.transform.position = weaponPoint.transform.position;
            weaponScript.spread /= 2;
        }
        else
        {
            anim.SetBool("IsCrouching", false);
            pawnCollider.height = 2;
            pawnCollider.center = new Vector3(pawnCollider.center.x, 1, pawnCollider.center.z);
            weaponPoint.transform.localPosition = new Vector3(weaponPoint.transform.localPosition.x, weaponPoint.transform.localPosition.y * 2, weaponPoint.transform.localPosition.z);
            equippedWeapon.transform.position = weaponPoint.transform.position;
            weaponScript.spread *= 2;
        }
    }

    public void Move(Vector3 direction)
    {
        anim.SetFloat("Vertical", direction.z);
        anim.SetFloat("Horizontal", direction.x);
    }

    public void OnEquip(GameObject newWeapon)
    {
        if (newWeapon.GetComponent<WeaponBase>().equipped == false)
        {
            Destroy(equippedWeapon);
            rightPoint = null;
            leftPoint = null;
            anim.SetInteger("CurrentWep", 0);
            equippedWeapon = newWeapon;
            equippedWeapon.layer = gameObject.layer;
            equippedWeapon.transform.parent = weaponPoint;
            equippedWeapon.transform.position = weaponPoint.transform.position;
            equippedWeapon.transform.rotation = weaponPoint.transform.rotation;
            weaponScript = equippedWeapon.GetComponent<WeaponBase>();
            weaponScript.OnEquip(this);
            weaponScript.equipped = true;
            anim.SetInteger("CurrentWep", (int)weaponScript.weaponType);
        }

        if (gameObject.layer == 9)
        {
            weaponImage.sprite = weaponSprites[(int)weaponScript.weaponType];
        }
    }

    public void OnAnimatorIK(int layerIndex)
    {
        if (!weaponScript)
        {
            return; //do nothing
        }
        if (weaponScript.rightHandTf)
        {
            anim.SetIKPosition(AvatarIKGoal.RightHand, rightPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.RightHand, rightPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);

            anim.SetIKPosition(AvatarIKGoal.LeftHand, leftPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, leftPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);

            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
        }
    }

    public void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag == "Weapon" && gameObject.layer == 9) {
            OnEquip(collider.gameObject);
        }
    }
}