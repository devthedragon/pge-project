﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropManager : MonoBehaviour
{

    public List<WeightedClass> dropList;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    GameObject randomItemSelect()
    {
        List<float> cDFArray = new List<float>();
        float totalRange = 0;
        for (int i = 0; i < dropList.Count; i++)
        {
            totalRange += dropList[i].chanceToSpawn;

            if (i != 0)
            {
                cDFArray[i] = dropList[i].chanceToSpawn + cDFArray[i - 1];
            }
            else
            {
                cDFArray[i] = dropList[i].chanceToSpawn;
            }
        }

        float randDrop = Random.Range(0, totalRange);

        int selectedIndex = System.Array.BinarySearch(cDFArray.ToArray(), randDrop);
        if (selectedIndex < 0)
        {
            selectedIndex = ~selectedIndex;

        }

        return dropList[selectedIndex].objectToSpawn;
    }
}
