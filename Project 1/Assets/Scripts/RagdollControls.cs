﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RagdollControls : MonoBehaviour {

    public GameObject objectToRagdoll;
    public Collider mainCollider;
    public Animator anim;
    public Rigidbody mainRigidbody;
    public NavMeshAgent agent;
    public List<Rigidbody> childRigidbody;
    public List<Collider> childCollider;
    public Rigidbody weaponRb;


	// Use this for initialization
	void Start () {
        mainCollider = objectToRagdoll.GetComponent<Collider>();
        anim = objectToRagdoll.GetComponent<Animator>();
        mainRigidbody = objectToRagdoll.GetComponent<Rigidbody>();
        agent = objectToRagdoll.GetComponent<NavMeshAgent>();

        childRigidbody = new List<Rigidbody>(objectToRagdoll.GetComponentsInChildren<Rigidbody>());
        childCollider = new List<Collider>(objectToRagdoll.GetComponentsInChildren<Collider>());

        DeactivateRagdoll();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ActivateRagdoll();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            DeactivateRagdoll();
        }
    }

    public void ActivateRagdoll()
    {
        weaponRb = GetComponent<Pawn>().equippedWeapon.GetComponent<Rigidbody>();
        foreach (Rigidbody rb in childRigidbody)
        {
            rb.isKinematic = false;
        }
        foreach (Collider collider in childCollider)
        {
            collider.enabled = true;
        }
        mainCollider.enabled = false;
        mainRigidbody.isKinematic = true;
        anim.enabled = false;
        agent.enabled = false;
        if (objectToRagdoll.GetComponent<Pawn>().equippedWeapon)
        {
            objectToRagdoll.GetComponent<Pawn>().equippedWeapon.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    public void DeactivateRagdoll()
    {
        foreach (Rigidbody rb in childRigidbody)
        {
            rb.isKinematic = true;
        }
        foreach (Collider collider in childCollider)
        {
            collider.enabled = false;
        }
        mainCollider.enabled = true;
        mainRigidbody.isKinematic = false;
        anim.enabled = true;
        agent.enabled = true;
        if (objectToRagdoll.GetComponent<Pawn>().equippedWeapon)
        {
            objectToRagdoll.GetComponent<Pawn>().equippedWeapon.GetComponent<Rigidbody>().isKinematic = true;
            objectToRagdoll.GetComponent<Pawn>().equippedWeapon.transform.position = objectToRagdoll.GetComponent<Pawn>().weaponPoint.position;
        }
    }
}
