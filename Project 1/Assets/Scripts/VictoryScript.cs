﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScript : MonoBehaviour {

    public float survivalTime;
    public GameObject objectToSpawn;
    public List<GameObject> spawnLocations = new List<GameObject>();
    private List<Transform> spawnTransforms = new List<Transform>();
    private Transform nextLocation;
    private GameObject spawnedObject;
    private float timeUntilNextSpawn;

    public Vector3 gizmoSize;
    public Color gizmoColor;

    public Text objectiveText;
    public bool objectiveSpawned = false;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < spawnLocations.Count; i++)
        {
            spawnTransforms.Add(spawnLocations[i].transform);
        }

        objectiveText = GameObject.Find("Objective Text").GetComponent<Text>();
        timeUntilNextSpawn = survivalTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeUntilNextSpawn > 0 && GameManager.instance.player)
        {
            objectiveText.text = "Survive for " + Mathf.Ceil(timeUntilNextSpawn) + " seconds.";
            timeUntilNextSpawn -= Time.deltaTime;
        }
        //if timer is done, spawns new object 
        else if (timeUntilNextSpawn <= 0 && objectiveSpawned == false)
        {
            Spawn();
            objectiveText.text = "Find the extraction point.";
        }
    }

    public void Spawn()
    {
        nextLocation = spawnTransforms[Random.Range(0, spawnLocations.Count - 1)];

        //create object
        spawnedObject = Instantiate(objectToSpawn, nextLocation.position + new Vector3(0, 1, 0), nextLocation.rotation);
        objectiveSpawned = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = gizmoColor;
        Gizmos.DrawCube(new Vector3(0, gizmoSize.y / 2, 0), gizmoSize);
        Gizmos.DrawRay(new Ray(Vector3.zero, Vector3.up));
    }
}
